module bc

go 1.16

require (
	go.etcd.io/bbolt v1.3.6
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
